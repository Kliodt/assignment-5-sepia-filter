make clean_pictures

echo ---------------------------------------------

echo "Тестируем программу без инструкций SSE"
start_time=$(date +"%s%3N") #<--time in milliseconds from 1.1.1970
#do testing tasks
for i in {1..10}
do
    out/program pictures/input.bmp pictures/test_picture_${i}_no_sse.bmp
done
#finish
end_time=$(date +"%s%3N")
time_interval=$(("$end_time - $start_time"))
echo "Прошло времени: $time_interval миллисекунд."

echo ---------------------------------------------

echo "Тестируем программу, исплользующую SSE"
start_time=$(date +"%s%3N")
#do testing tasks
for i in {1..10}
do
    out/program pictures/input.bmp pictures/test_picture_${i}_sse.bmp -use-sse
done
#finish
end_time=$(date +"%s%3N")
time_interval=$(("$end_time - $start_time"))
echo "Прошло времени: $time_interval миллисекунд."

echo ---------------------------------------------
