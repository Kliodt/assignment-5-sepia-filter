#include "../include/formats.h"
#include "../include/image.h"
#include "../include/status_codes.h"
#include "../include/transformations.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SSE_MODE_FLAG "-use-sse"

enum process_status process_filter(char* file_in_name, char* file_out_name, char* mode) {
    
    FILE* file_in = fopen(file_in_name, "rb");
    FILE* file_out = fopen(file_out_name, "w");

    if (file_in == NULL) {
        return PROCESS_READ_FILE_ERROR;
    }

    image img = {0};
    if (read_bmp(file_in, &img) != READ_IMAGE_OK) {
        return PROCESS_PARSE_IMAGE_ERROR;
    }
    
    image* new;
    if (strcmp(SSE_MODE_FLAG, mode) == 0) { //equal
        new = apply_sepia_asm(&img);
    }
    else {
        new = apply_sepia(&img);
    }

    if (write_bmp(file_out, new) != WRITE_IMAGE_OK) {
        return PROCESS_WRITE_FILE_ERROR;
    }

    free(new->data);

    fclose(file_in);
    fclose(file_out);

    return PROCESS_OK;
}

//Input format: ./(executable) <source-image> <transformed-image> [<-use-sse>]"
int main( int argc, char** argv ) {
    if (argc < 3) {
        fprintf(stderr, "argument amount error");
        return -1;
    }
    enum process_status status = process_filter(argv[1], argv[2], argc > 3 ? argv[3] : "");

    switch (status) {
        case PROCESS_READ_FILE_ERROR: {
            fprintf(stderr, "error in reading file");
            break;
        }
        case PROCESS_WRITE_FILE_ERROR: {
            fprintf(stderr, "error in writing to file");
            break;
        }
        case PROCESS_PARSE_IMAGE_ERROR: {
            fprintf(stderr, "error in parsing image");
            break;
        }
        default: {}
    }

    return 0;
}


