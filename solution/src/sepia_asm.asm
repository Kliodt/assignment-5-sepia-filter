global apply_sepia_asm

section .data
    align 16
    col_1: dd 0.393, 0.349, 0.272, 0.0
    col_2: dd 0.769, 0.686, 0.534, 0.0
    col_3: dd 0.189, 0.168, 0.131, 0.0


section .text
    apply_sepia_asm:
        sub rsp, 16
        mov rdx, [rdi+8]            ;image height
        mov rsi, [rdi+16]           ;first pixel address
        movaps xmm2, [col_1]
        movaps xmm4, [col_2]
        movaps xmm6, [col_3]

        .cycle_for_height:
            test rdx, rdx
            jz .cycle_for_height_break

            mov rcx, [rdi]                      ;image width
            .cycle_for_width:
                test rcx, rcx
                jz .cycle_for_width_break

                ;values for current pixel b=[rsi], g=[rsi+1], r=[rsi+2]
                
                
                mov al, [rsi+2]
                shl rax, 32
                mov al, [rsi+2]
                pinsrq xmm1, rax, 0
                pinsrq xmm1, rax, 1
                ;CVTDQ2PS xmm1, xmm1

                mov al, [rsi+1]
                shl rax, 32
                mov al, [rsi+1]
                pinsrq xmm3, rax, 0
                pinsrq xmm3, rax, 1
                ;CVTDQ2PS xmm3, xmm3

                mov al, [rsi]
                shl rax, 32
                mov al, [rsi]
                pinsrq xmm5, rax, 0
                pinsrq xmm5, rax, 1
                ;CVTDQ2PS xmm5, xmm5

                mulps xmm1, xmm2
                mulps xmm3, xmm4
                addps xmm3, xmm1
                mulps xmm5, xmm6
                addps xmm5, xmm3

                ;CVTPS2DQ xmm5, xmm5

                movups [rsp-16], xmm5

                mov ax, [rsp-16]
                cmp ax, 255
                jl .m1
                mov ax, 255
                .m1: mov [rsi+2], al

                mov ax, [rsp-12]
                cmp ax, 255
                jl .m2
                mov ax, 255
                .m2: mov [rsi+1], al

                mov al, [rsp-8]
                cmp ax, 255
                jl .m3
                mov ax, 255
                .m3: mov [rsi], al




                add rsi, 3                  ;where 3==sizeof(pixel)
                dec rcx
                jmp .cycle_for_width
            .cycle_for_width_break:

            dec rdx
            jmp .cycle_for_height
        .cycle_for_height_break:

        mov rax, rdi   
        add rsp, 16     
        ret
