#include "../include/image.h"

#include <malloc.h>
#include <string.h>


static int to_valid_pixel(int value) {
    return value > MAX_PIXEL_VALUE ? MAX_PIXEL_VALUE : value;
}

static struct pixel calculate_sepied_pixel(struct pixel old) {
    int tr = 0.393f * old.r + 0.769f * old.g + 0.189f * old.b;
    int tg = 0.349f * old.r + 0.686f * old.g + 0.168f * old.b;
    int tb = 0.272f * old.r + 0.534f * old.g + 0.131f * old.b;
    return (struct pixel){.r = to_valid_pixel(tr), .b = to_valid_pixel(tb), .g = to_valid_pixel(tg)};
}

// returns the same image but with sepia
image* apply_sepia(image* old) {
    for (size_t i = 0; i < old->height; i++) {
        for (size_t j = 0; j < old->width; j++) {
            old->data[old->width * i + j] = calculate_sepied_pixel(old->data[old->width * i + j]);
        }
    }
    return old;
}