#ifndef IMAGE_TRANSFORMER_TRANSFORMATIONS_H
#define IMAGE_TRANSFORMER_TRANSFORMATIONS_H

#include "image.h"

image* apply_sepia(image* old);

image* apply_sepia_asm(image* img);

#endif //IMAGE_TRANSFORMER_TRANSFORMATIONS_H
