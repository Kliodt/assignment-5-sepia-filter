#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>

#define SIZE_OF_ONE_PIXEL sizeof(struct pixel)

#define MAX_PIXEL_VALUE 255

typedef struct __attribute__((packed)) image {
    uint64_t width, height;
    struct pixel* data;
} image;

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

#endif //IMAGE_TRANSFORMER_IMAGE_H
