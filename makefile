CC = gcc
CC_FLAGS = -no-pie
ASM = nasm
ASM_FLAGS = -w+error=all -felf64

SRC_DIR = ./solution/src/
BUILD_DIR = ./build/
OUT_DIR = ./out/
PICTURES_DIR = ./pictures/

SOURCES_C = $(shell find $(SRC_DIR) -type f -name *.c)
SOURCES_ASM = $(shell find $(SRC_DIR) -type f -name *.asm)
TARGET = program

#build asm file
%.o: $(SRC_DIR)%.asm
	$(ASM) $(ASM_FLAGS) $^ -o $(BUILD_DIR)$@

#build final program
$(TARGET): $(SOURCES_C) $(SOURCES_ASM)
	$(CC) $(CC_FLAGS) $(SOURCES_C) $(BUILD_DIR)*.o -o $(OUT_DIR)$@

#clean build and out directories
clean:
	rm -rf $(OUT_DIR)*
	rm -rf $(BUILD_DIR)*

clean_pictures:
	rm -f $(PICTURES_DIR)test* 